from copy import copy, deepcopy
from task import generate_stock_updates_pdf


FAKE_DATE = "2023-04-21T06:50:00"
FAKE_DATA: dict = {
    "image_name": "623843.jpg",
    "category": "Плейты",
    "id": "14769pb320",
    "vendor_code": 12345678,
    "color": "Прозрачнно-оранжевый неон",
    "count": 10_000,
    "price": "1000.00",
}
FAKE_MESSAGE_LIST: dict = {
    "date": copy(FAKE_DATE),
    "data": [deepcopy(FAKE_DATA) for i in range(10)]
}
FAKE_DATA["category"] = "Тайлы"
for i in range(15):
    FAKE_MESSAGE_LIST["data"].append(deepcopy(FAKE_DATA))

if __name__ == "__main__":
    generate_stock_updates_pdf(deepcopy(FAKE_MESSAGE_LIST))
