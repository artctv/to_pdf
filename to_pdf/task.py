import shutil
import base64
from typing import Optional
from dataclasses import dataclass
from copy import copy
import jinja2
import pdfkit
from config import Config


@dataclass
class MessageData:
    image_name: str
    category: str
    category_link: str
    id: str
    vendor_code: int  # code ака артикул
    color: str
    count: str
    price: str
    order_link: str
    order: Optional[int] = None


@dataclass
class MessageList:
    date: str
    data: list[MessageData]


def generate_context(data: dict):
    CSS_NAME: str = "stock_updates.css"  # noqa
    date = data["date"]

    context = {
        "static": Config.STATIC_DIR / CSS_NAME,
        "date": date.split("T")[0].replace("-", ".")
    }
    with open(Config.STATIC_DIR / "logo.png", "rb") as f:
        context["logo"] = base64.b64encode(f.read()).decode()

    categories, categories_data = [], {}
    for i in data["data"]:
        category = i.pop("category")
        categories.append(copy(category))

        if category not in categories_data:
            categories_data[copy(category)] = []

        image_path = Config.IMAGES_DIR / date /i.pop("image_name")
        result_dict = copy(i)
        with open(image_path, "rb") as f:
            result_dict["image"] = base64.b64encode(f.read()).decode()

        categories_data[category].append(copy(result_dict))

    context["categories"] = list(set(categories))
    context["categories_data"] = categories_data
    return context


def generate_stock_updates_pdf(data: dict):
    TEMPLATE_NAME: str = "stock_updates.html"  # noqa
    pdfkit_config = pdfkit.configuration(wkhtmltopdf=Config.WKHTMLTOPDF_PATH)
    context: dict = generate_context(data)
    template_loader = jinja2.FileSystemLoader(str(Config.TEMPLATES_DIR))
    env = jinja2.Environment(loader=template_loader)
    template = env.get_template(str(TEMPLATE_NAME))
    output = template.render(context)
    pdfkit.from_string(
        output,
        str(Config.PDF_DIR/data["date"])+".pdf",
        configuration=pdfkit_config,
    )

    # удаляем папку с картинками что бы не захламлять, раскоментить на проде
    # shutil.rmtree(Config.IMAGES_DIR / data["date"], ignore_errors=True)

    # with open(Config.BASE_DIR / "index.html", "w") as f:  # для тестирование стиля записаь в index.html в корне
    #     f.write(str(output))

