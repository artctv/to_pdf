import pathlib


class Config:
    """
        Simple config data

        Folders structure:
        - files/                         # файлы для таски и результат таски
            - pdf/                # готовые pdf файлы
                - 2023-04-21T06:50:00.pdf   # готовый pdf файл названный по имени аналогичному папки с таской
            - images/                   # картинки для генерации pdf
                - 2023-04-21T06:50:00/  # папка с картинками для таски в виде: UTC время в момент запуска таски
                    img1.jpeg            # в формате ISO, без милисекунд и секунд 00
                    img2.jpeg            # код для такого формата:
                                        `datetime.datetime.utcnow().replace(microsecond=0, second=0).isoformat()`

        - to_pdf/           # код таски
            - templates     # html шаблоны
            - static        # css стили
            - *.py          # код

    """
    # dir pathes: out of code folders
    BASE_DIR: pathlib.Path = pathlib.Path(__file__).parent.parent.resolve()
    FILES_DIR: pathlib.Path = BASE_DIR / "files"
    IMAGES_DIR: pathlib.Path = FILES_DIR / "images"
    PDF_DIR: pathlib.Path = FILES_DIR / "pdf"

    # dir pathes: folders near code
    PROJECT_DIR: pathlib.Path = pathlib.Path(__file__).parent.resolve()
    TEMPLATES_DIR: pathlib.Path = PROJECT_DIR / "templates"
    STATIC_DIR: pathlib.Path = PROJECT_DIR / "static"

    WKHTMLTOPDF_PATH: str = "/usr/local/bin/wkhtmltopdf"
