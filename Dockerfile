FROM python:3.9.15-slim as builder

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y xz-utils wget libfontconfig1 libxrender1 libxext6

RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN tar -vxf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN cp wkhtmltox/bin/wk* /usr/local/bin/

#ENV wkhtmltopdf_path /wkhtmltox/bin/wkhtmltopdf

FROM builder as app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

WORKDIR app
VOLUME ./files /files

COPY to_pdf/ .


CMD ["python", "run.py"]